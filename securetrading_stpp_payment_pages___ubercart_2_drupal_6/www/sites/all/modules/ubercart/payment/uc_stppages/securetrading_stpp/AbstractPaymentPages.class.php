<?php

/**
 * SecureTrading STPP Shopping Carts
 * STPP Cart Interface Version 1.0.0
 * Last updated 19/10/2011
 * Written by Peter Barrow for SecureTrading Ltd.
 * http://www.securetrading.com
 */
 
abstract class AbstractPaymentPages extends AbstractSTPP
{
	private $version = 1;
	
	protected static $url = 'https://payments.securetrading.net/process/payments/choice';
	
	public function __construct() {
		return parent::__construct();
	}
	
	final public static function formatPrice($amount, $currency = '') {
		$amount = (float) $amount;
		return sprintf('%.2f', $amount);
	}
	
	final public static function returnUrl() {
		return self::$url;
	}
	
	/**
	 * If a shopping cart does not call self::runPaymentPages() to access SecureTrading, it must call this method manually to validate/format the data correctly.
	 */
	final public function preChecks(stdClass &$request_object) {
	
		// Assign the settleduedate and settlestatus fields default values if they have not been set:
		$request_object->settleduedate = isset($request_object->settleduedate) ? $request_object->settleduedate : 0;
		$request_object->settlestatus = isset($request_object->settlestatus) ? $request_object->settlestatus : 0;
		 
		// Ensure the settleduedate and the settlestatus provided are both valid:
		$this->checkSettleDueDate($request_object->settleduedate);
		$this->checkSettleStatus($request_object->settlestatus);
		
		// Format the settleduedate:
		$request_object->settleduedate = $this->formatSettleDueDate($request_object->settleduedate);
		
		// Add the version to the request object:
		$request_object->version = $this->version;
	}
	
	/**
	 * This method can be used to create the hidden input fields of a form in a cart's template file.
	 **/
	final public function returnHiddenFields($request_object) {
		$string = '';
		$postData = get_object_vars($request_object);
		
		foreach($postData as $k => $v) {
			$string .= "<input type='hidden' name='$k' value='$v' />\n";
		}
		return $string;
	}
	
	/**
	 * This method can be called to create a form that submits to SecureTrading.
	 * It will call self::preChecks() automatically to check/format the passed data, and self::returnHiddenFields() to create the hidden input fields of the form.
	 * @param $requestObject stdClass The information to be passed to SecureTrading.
	 * @param $autoSubmit bool Set to TRUE in order to add a JS call to make the form auto-submit.  Set to FALSE to create an ordinary form.
	 */
	final public function runPaymentPages(stdClass $request_object, $autoSubmit = TRUE)
	{
		$this->preChecks($request_object);
		$url = self::$url;
		
		if ($autoSubmit === TRUE) {
			echo "<body onLoad='javascript: document.process.submit();'>\n\n";
		}
		
		echo "<form method='post' action='{$url}' name='process' enctype='multipart/form-data' accept-charset='UTF-8'>\n";
		
		echo $this->returnHiddenFields($request_object);
		
		if ($autoSubmit === TRUE) {
			echo "
				{$this->languageVars['js_disabled']}
			";
		}
	}
	
	final public function createException(Exception $e, $file, $class, $line)
	{
		$this->logError($e->getMessage(), 2, $file, $class, $line);
		$message = $this->languageVars['unexpected_error'];
		$this->handleError($message, $this->errorParams);
	}
	
	public function getUrl() {
		return self::$url;
	}
	
	static public function createHash(&$requestObject, $storePass) {
		$requestObject->sitesecurity = hash('sha256', $requestObject->sitereference . $requestObject->currencyiso3a . $requestObject->mainamount . $requestObject->orderreference . $storePass);
	}
}

?>