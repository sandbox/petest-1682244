<?php

require_once(dirname(__FILE__) . '/securetrading_stpp/STPPLoader.php');
require_once(dirname(__FILE__) . '/UberCart24PPages.class.php');

function uc_stppages_notification($cart_id = 0) {
  
  $ob = new UberCart24PPages();
  
  // Ensure responsesitesecurity has been returned if it is enabled in the configuration:
  if (!isset($_POST['responsesitesecurity']) && variable_get('uc_stppages_use_notification', '')) {
    $ob->createException(new Exception('The responsesitesecurity was not returned but the notification hash was enabled.'), __FILE__, __CLASS__, __LINE__);
  }
  
  // Ensure orderreference has been returned:
  if (!isset($_POST['orderreference']) || !is_string($_POST['orderreference'])) {
	$ob->createException(new Exception('The orderreference was not returned to the notification script.'), __FILE__, __CLASS__, __LINE__);
  }
  
  // Ensure errorcode has been returned:
   if (!isset($_POST['errorcode']) || !is_string($_POST['errorcode'])) {
	$ob->createException(new Exception('The errorcode was not returned to the notification script.'), __FILE__, __CLASS__, __LINE__);
  }
  
  // Ensure all AVS information has been returned:
  if (!isset($_POST['securityresponsesecuritycode']) || !isset($_POST['securityresponseaddress']) || !isset($_POST['securityresponsepostcode'])) {
    $ob->createException(new Exception('Invalid AVS information was returned to the notification.'), __FILE__, __CLASS__, __LINE__);
  }
  
  // If responsesitesecurity was required, ensure its hash matches the generated hash:
  if (isset($_POST['responsesitesecurity']) && hash('sha256', $_POST['errorcode'] . $_POST['orderreference'] . $_POST['securityresponseaddress'] . $_POST['securityresponsepostcode'] . $_POST['securityresponsesecuritycode'] . variable_get('uc_stppages_notification', '')) != $_POST['responsesitesecurity']) {
	$ob->createException(new Exception('The responsesitesecurity did not match the generated notification hash.'), __FILE__, __CLASS__, __LINE__);
  }
  
  $order = uc_order_load($_POST['orderreference']);

  if ($order === FALSE || uc_order_status_data($order->order_status, 'state') == 'post_checkout') { // If the order has not already been marked as paid, we can update its status:
    $ob->createException(new Exception('The order object is false, or the order is in "post checkout".'), __FILE__, __CLASS__, __LINE__);
  }

  unset($ob);
  
  $avsString = "Security: " . $_POST['securityresponsesecuritycode'] . " Address: " . $_POST['securityresponseaddress'] . " Postcode: " . $_POST['securityresponsepostcode'];
  $amount = UberCart24PPages::formatPrice($order->order_total, variable_get('uc_currency_code', ''));
  
  switch($_POST['errorcode']) {
	case "0":
	  uc_payment_enter($order->order_id, 'stppages', $amount, NULL, NULL, $avsString); // Last param is for order comments.
      uc_order_save($order);
      uc_order_comment_save($order->order_id, $amount, t('Order created through website.'), 'admin');
      uc_cart_complete_sale($order, variable_get('uc_new_customer_login', FALSE));
	  uc_order_update_status($order->order_id, uc_order_state_default('post_checkout')); // The order won't be updated in complete_sale if it had previously been set to decline....
	  break;
	case "70000":
	default:
	  uc_order_update_status($order->order_id, uc_order_state_default('canceled'));
  }
}