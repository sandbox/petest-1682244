<?php

class UberCart24PPages extends AbstractPaymentPages
{
	public function __construct() {
		$this->logDirectory = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'securetrading_stpp' . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR;
	}
	
     protected function handleError($message, $params) {
	   drupal_set_message($message, 'error');
	   drupal_goto('cart/checkout/review');
     }
}

?>