<?php

/**
 * SecureTrading STPP Shopping Carts
 * STPP Cart Interface Version 1.0.0
 * Last updated 19/10/2011
 * Written by Peter Barrow for SecureTrading Ltd.
 * http://www.securetrading.com
 */
 
abstract class AbstractSTPPHelper
{
	protected $cartObject;
	
	public function __construct(AbstractSTPP $object) {
		$this->cartObject = $object;
	}
	
	abstract public function retrieveCartVersion();
}

?>