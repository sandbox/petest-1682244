<?php

/**
 * SecureTrading STPP Shopping Carts
 * STPP Cart Interface Version 1.0.0
 * Last updated 10/10/2011
 * Written by Peter Barrow for SecureTrading Ltd.
 * http://www.securetrading.com
 */
 
class STPP_XML
{
	private $requestObject;
	
	function __construct(stdClass $requestObject) {
		$this->requestObject = $requestObject;
	}
	
	function formatRequest()
	{
		$args = $this->requestObject;
		
		$vars['api_version'] = isset($args->apiversion) ? $args->apiversion : '';
		$vars['site_reference'] = isset($args->sitereference) ? $args->sitereference : '';
		$vars['alias'] = isset($args->alias) ? $args->alias : '';
		$vars['order_reference'] = isset($args->orderreference) ? $args->orderreference : '';
		$vars['ip'] = isset($args->ip) ? $args->ip : '';
		$vars['currency_code'] = isset($args->currencycode) ? $args->currencycode : '';
		$vars['amount'] = isset($args->amount) ? $args->amount : '';
		$vars['settle_due_date'] = isset($args->settleduedate) ? $args->settleduedate : '';
		$vars['settle_status'] = isset($args->settlestatus) ? $args->settlestatus : '';
		$vars['account_type_description'] = isset($args->accounttypedescription) ? $args->accounttypedescription : '';
		$vars['parent_transaction_reference'] = isset($args->parenttransactionreference) ? $args->parenttransactionreference : '';
		$vars['term_url'] = isset($args->termurl) ? $args->termurl : '';
		$vars['md'] = isset($args->md) ? $args->md : '';
		$vars['pares'] = isset($args->pares) ? $args->pares : '';
		$vars['transaction_reference'] = isset($args->transactionreference) ? $args->transactionreference : '';
		$vars['settle_base_amount'] = isset($args->settlebaseamount) ? $args->settlebaseamount : '';
		
		$vars['customer_telephone_type'] = isset($args->customerteltype) ? $args->customerteltype : '';
		$vars['customer_telephone_number'] = isset($args->customertelno) ? $args->customertelno : '';
		$vars['customer_county'] = isset($args->customercounty) ? $args->customercounty : '';
		$vars['customer_street'] = isset($args->customerstreet) ? $args->customerstreet : '';
		$vars['customer_postcode'] = isset($args->customerpostcode) ? $args->customerpostcode : '';
		$vars['customer_premise'] = isset($args->customerpremise) ? $args->customerpremise : '';
		$vars['customer_town'] = isset($args->customertown) ? $args->customertown : '';
		$vars['customer_country'] = isset($args->customercountry) ? $args->customercountry : '';
		$vars['customer_middle_name'] = isset($args->customermiddlename) ? $args->customermiddlename : '';
		$vars['customer_prefix'] = isset($args->customerprefix) ? $args->customerprefix : '';
		$vars['customer_last_name'] = isset($args->customerlastname) ? $args->customerlastname : '';
		$vars['customer_first_name'] = isset($args->customerfirstname) ? $args->customerfirstname : '';
		$vars['customer_suffix'] = isset($args->customersuffix) ? $args->customersuffix : '';
		$vars['customer_email'] = isset($args->customeremail) ? $args->customeremail : '';
		
		$vars['billing_telephone_type'] = isset($args->billingteltype) ? $args->billingteltype : '';
		$vars['billing_telephone_number'] = isset($args->billingtelno) ? $args->billingtelno : '';
		$vars['billing_county'] = isset($args->billingcounty) ? $args->billingcounty : '';
		$vars['billing_street'] = isset($args->billingstreet) ? $args->billingstreet : '';
		$vars['billing_postcode'] = isset($args->billingpostcode) ? $args->billingpostcode : '';
		$vars['billing_premise'] = isset($args->billingpremise) ? $args->billingpremise : '';
		$vars['billing_town'] = isset($args->billingtown) ? $args->billingtown : '';
		$vars['billing_country'] = isset($args->billingcountry) ? $args->billingcountry : '';
		$vars['billing_middle_name'] = isset($args->billingmiddlename) ? $args->billingmiddlename : '';
		$vars['billing_prefix'] = isset($args->billingprefix) ? $args->billingprefix : ''; # prefix not in billing?
		$vars['billing_last_name'] = isset($args->billinglastname) ? $args->billinglastname : '';
		$vars['billing_first_name'] = isset($args->billingfirstname) ? $args->billingfirstname : '';
		$vars['billing_suffix'] = isset($args->billingsuffix) ? $args->billingsuffix : '';
		$vars['billing_email'] = isset($args->billingemail) ? $args->billingemail : '';
		
		$vars['payment_type'] = isset($args->paymenttype) ? $args->paymenttype : '';
		$vars['start_date'] = isset($args->startdate) ? $args->startdate : '';
		$vars['expiry_date'] = isset($args->expirydate) ? $args->expirydate : '';
		$vars['pan'] = isset($args->pan) ? $args->pan : '';
		$vars['security_code'] = isset($args->securitycode) ? $args->securitycode : '';
		$vars['issue_number'] = isset($args->issuenumber) ? $args->issuenumber : '';
		
		switch(strtolower($args->requesttype)) {
			case 'auth':
				return $this->formatAuth($vars);
			case 'threedquery':
				return $this->format3dQuery($vars);
			case 'threedauth_enrolled':
				return $this->format3dAuthEnrolled($vars);
			case 'threedauth_notenrolled':
				return $this->format3dAuthNotEnrolled($vars);
			case 'refund':
				return $this->formatRefund($vars);
			case 'updaterefund':
				return $this->formatUpdateRefund($vars);
			case 'updatepartialrefund':
				return $this->formatUpdatePartialRefund($vars);
			default:
				throw new Exception('An invalid request type was specified.');
		}
	}
	
	private function formatAuth($vars)
	{
		extract($vars);
		
		return "
			<?xml version='1.0' encoding='utf-8' ?>
			<requestblock version='$api_version'>
			  <alias>$alias</alias>
			  <request type='AUTH'>
				<merchant>
				  <orderreference>$order_reference</orderreference>
				</merchant>
				<customer>
				  <ip>$ip</ip>
				  <telephone type='$customer_telephone_type'>$customer_telephone_number</telephone>
				  <street>$customer_street</street>
				  <postcode>$customer_postcode</postcode>
				  <premise>$customer_premise</premise>
				  <town>$customer_town</town>
				  <country>$customer_country</country>
				  <name>
					<middle>$customer_middle_name</middle>
					<prefix>$customer_prefix</prefix>
					<last>$customer_last_name</last>
					<first>$customer_first_name</first>
					<suffix>$customer_suffix</suffix>
				  </name>
				  <email>$customer_email</email>
				</customer>
				<billing>
				  <telephone type='$billing_telephone_type'>$billing_telephone_number</telephone>
				  <county>$billing_county</county>
				  <street>$billing_street</street>
				  <postcode>$billing_postcode</postcode>
				  <premise>$billing_premise</premise>
				  <town>$billing_town</town>
				  <country>$billing_country</country>
				  <payment type='$payment_type'>
					<startdate>$start_date</startdate>
					<expirydate>$expiry_date</expirydate>
					<pan>$pan</pan>
					<securitycode>$security_code</securitycode>
					<issuenumber>$issue_number</issuenumber>
				  </payment>
				  <name>
					<middle>$billing_middle_name</middle>
					<prefix>$billing_prefix</prefix>
					<last>$billing_last_name</last>
					<suffix>$billing_suffix</suffix>
					<first>$billing_first_name</first>
				  </name>
				  <amount currencycode='$currency_code'>$amount</amount>
				  <email>$billing_email</email>
				</billing>
				<operation>
				  <sitereference>$site_reference</sitereference>
				  <accounttypedescription>$account_type_description</accounttypedescription>
				</operation>
				<settlement>
					<settleduedate>$settle_due_date</settleduedate>
					<settlestatus>$settle_status</settlestatus>
				</settlement>
			  </request>
			</requestblock>
		";
	}
	
	private function format3dQuery($vars)
	{
		extract($vars);
		
		return "
			<?xml version='1.0' encoding='utf-8'?>
			<requestblock version='$api_version'>
			  <alias>$alias</alias>
			  <request type='THREEDQUERY'>
				<merchant>
				  <orderreference>$order_reference</orderreference>
				  <termurl>$term_url</termurl>
				</merchant>
				<customer>
				  <ip>$ip</ip>
				  <telephone type='$customer_telephone_type'>$customer_telephone_number</telephone>
				  <street>$customer_street</street>
				  <postcode>$customer_postcode</postcode>
				  <premise>$customer_premise</premise>
				  <town>$customer_town</town>
				  <country>$customer_country</country>
				  <name>
					<middle>$customer_middle_name</middle>
					<prefix>$customer_prefix</prefix>
					<last>$customer_last_name</last>
					<first>$customer_first_name</first>
					<suffix>$customer_suffix</suffix>
				  </name>
				  <email>$customer_email</email>
				</customer>
				<billing>
				  <telephone type='$billing_telephone_type'>$billing_telephone_number</telephone>
				  <county>$billing_county</county>
				  <street>$billing_street</street>
				  <postcode>$billing_postcode</postcode>
				  <premise>$billing_premise</premise>
				  <town>$billing_town</town>
				  <country>$billing_country</country>
				  <payment type='$payment_type'>
					<startdate>$start_date</startdate>
					<expirydate>$expiry_date</expirydate>
					<pan>$pan</pan>
					<securitycode>$security_code</securitycode>
					<issuenumber>$issue_number</issuenumber>
				  </payment>
				  <name>
					<middle>$billing_middle_name</middle>
					<prefix>$billing_prefix</prefix>
					<last>$billing_last_name</last>
					<suffix>$billing_suffix</suffix>
					<first>$billing_first_name</first>
				  </name>
				  <amount currencycode='$currency_code'>$amount</amount>
				  <email>$billing_email</email>
				</billing>
				<operation>
				  <sitereference>$site_reference</sitereference>
				  <accounttypedescription>$account_type_description</accounttypedescription>
				</operation>
				<settlement>
					<settleduedate>$settle_due_date</settleduedate>
					<settlestatus>$settle_status</settlestatus>
				</settlement>
			  </request>
			</requestblock>
		";	
	}
	
	private function format3dAuthEnrolled($vars)
	{
		extract($vars);
		
		return "
			<?xml version='1.0' encoding='utf-8'?>
			<requestblock version='$api_version'>
			<alias>$alias</alias>
			<request type='AUTH'>
				<operation>
					<md>$md</md>
					<pares>$pares</pares>
				</operation>
			</request>
			</requestblock>
		";
	}
	
	private function format3dAuthNotEnrolled($vars)
	{
		extract($vars);
		
		return "
			<?xml version='1.0' encoding='utf-8'?>
			<requestblock version='$api_version'>
			<alias>$alias</alias>
			<request type='AUTH'>
				<operation>
					<sitereference>$site_reference</sitereference>
					<parenttransactionreference>$parent_transaction_reference</parenttransactionreference>
				</operation>
			</request>
			</requestblock>
		";
	}
	
	private function formatRefund($vars)
	{
		extract($vars);
		
		return "
			<?xml version='1.0' encoding='utf-8'?>
			<requestblock version='$api_version'>
				<alias>$alias</alias>
				<request type='REFUND'>
					<merchant>
						<orderreference>$order_reference</orderreference>
					</merchant>
					<operation>
						<sitereference>$site_reference</sitereference>
						<parenttransactionreference>$parent_transaction_reference</parenttransactionreference>
					</operation>
					<billing>
						<amount currencycode='$currency_code'>$amount</amount>
					</billing>
				</request>
			</requestblock>
		";
	}
	
	/**
	 * Special case that processes a full or partial refund or cancels the transaction if it has not been settled.
	 * Settle status should always be '3' in the update since we want to cancel the transaction.
	 */
	private function formatUpdateRefund($vars)
	{
		extract($vars);
		
		return "
			<?xml version='1.0' encoding='utf-8'?>
			<requestblock version='$args->apiversion'>
				<alias>$args->alias</alias>
				<request type='TRANSACTIONUPDATE'>
					<filter>
						<sitereference>$site_reference</sitereference>
						<transactionreference>$transaction_reference</transactionreference>
					</filter>
					<updates>
						<settlement>
							<settlestatus>3</settlestatus>
						</settlement>
					</updates>
				</request>
				<request type='REFUND'>
					<operation>
						<sitereference>$site_reference</sitereference>
					</operation>
				</request>
			</requestblock>
		";
	}
	
	/**
	 * Refunds part of the authorized amount; we do not want to change the settle status here.
	 */
	private function formatUpdatePartialRefund($vars)
	{
		extract($vars);
		
		return "
			<?xml version='1.0' encoding='utf-8'?>
			<requestblock version='$api_version'>
				<alias>$alias</alias>
				<request type='TRANSACTIONUPDATE'>
					<filter>
						<sitereference>$site_reference</sitereference>
						<transactionreference>$transaction_reference</transactionreference>
					</filter>
					<updates>
						<settlement>
							<settlebaseamount>$args->settlebaseamount</settlebaseamount>
						</settlement>
					</updates>
				</request>
				<request type='REFUND'>
					<operation>
						<sitereference>$site_reference</sitereference>
					</operation>
					<billing>
						<amount>$amount</amount>
					</billing>
				</request>
			</requestblock>
		";
	}
	
	function buildResponseObject($xmlResponse)
	{
		$ro = new stdClass;
		
		switch($xmlResponse->response->attributes()->type) {
		
			case "THREEDQUERY":	$ro->requestreference = (string)$xmlResponse->requestreference;
								$ro->type = "THREEDQUERY";
								$ro->merchantname = (string) $xmlResponse->response->merchant->merchantname;
								$ro->orderreference = (string) $xmlResponse->response->merchant->orderreference;
								$ro->tid = (string) $xmlResponse->response->merchant->tid;
								$ro->merchantnumber = (string) $xmlResponse->response->merchant->merchantnumber;
								$ro->merchantcountryiso2a = (string) $xmlResponse->response->merchant->merchantcountryiso2a;
								$ro->transactionreference = (string) $xmlResponse->response->transactionreference;
								$ro->cardtype = (string) $xmlResponse->response->billing->payment->attributes()->type;
								$ro->maskedpan = (string) $xmlResponse->response->billing->payment->pan;
								$ro->timestamp = (string) $xmlResponse->response->timestamp;
								$ro->acsurl = (string) $xmlResponse->response->threedsecure->acsurl;
								$ro->md = (string) $xmlResponse->response->threedsecure->md;
								$ro->xid = (string) $xmlResponse->response->threedsecure->xid;
								$ro->pareq = (string) $xmlResponse->response->threedsecure->pareq;
								$ro->enrolled = (string) $xmlResponse->response->threedsecure->enrolled;
								$ro->live = (string) $xmlResponse->response->live;
								$ro->errorcode = (string) $xmlResponse->response->error->code;
								$ro->errormessage = (string) $xmlResponse->response->error->message;
								$ro->accounttypedescription = (string) $xmlResponse->response->operation->accounttypedescription;
								$ro->settleduedate = (string) $xmlResponse->response->settlement->settleduedate;
								$ro->settlestatus = (string) $xmlResponse->response->settlement->settlestatus;
								break;
			
			case "AUTH":		$ro->requestreference = (string) $xmlResponse->requestreference;
								$ro->type = "AUTH";
								$ro->merchantname = (string) $xmlResponse->response->merchant->merchantname;
								$ro->orderreference = (string) $xmlResponse->response->merchant->orderreference;
								$ro->tid = (string) $xmlResponse->response->merchant->tid;
								$ro->merchantnumber = (string) $xmlResponse->response->merchant->merchantnumber;
								$ro->merchantcountryiso2a = (string) $xmlResponse->response->merchant->merchantcountryiso2a;
								$ro->transactionreference = (string) $xmlResponse->response->transactionreference;
								$ro->securitycode = (string) $xmlResponse->response->security->securitycode;  #####
								$ro->securitypostcode = (string) $xmlResponse->response->security->postcode; ##
								$ro->securityaddress = (string) $xmlResponse->response->security->address; ###
								$ro->amount = (string) $xmlResponse->response->billing->amount; ###
								$ro->currencycode = (string) $xmlResponse->response->billing->amount->attributes()->currencycode; ##
								$ro->cardtype = (string) $xmlResponse->response->billing->payment->attributes()->type;
								$ro->maskedpan = (string) $xmlResponse->response->billing->payment->pan;
								$ro->authcode = (string) $xmlResponse->response->authcode; ###
								$ro->timestamp = (string) $xmlResponse->response->timestamp;
								$ro->threedcavv = (string) $xmlResponse->response->threedsecure->cavv;
								$ro->threedstatus = (string) $xmlResponse->response->threedsecure->status;
								$ro->threedxid = (string) $xmlResponse->response->threedsecure->xid;
								$ro->threedeci = (string) $xmlResponse->response->threedsecure->eci;
								$ro->enrolled = (string) $xmlResponse->response->threedsecure->enrolled;
								$ro->live = (string) $xmlResponse->response->live;
								$ro->errorcode = (string) $xmlResponse->response->error->code;
								$ro->errormessage = (string) $xmlResponse->response->error->message;
								$ro->parenttransactionreference = (string) $xmlResponse->response->operation->parenttransactionreference;
								$ro->accounttypedescription = (string) $xmlResponse->response->operation->accounttypedescription;
								$ro->settleduedate = (string) $xmlResponse->response->settlement->settleduedate;
								$ro->settlestatus = (string) $xmlResponse->response->settlement->settlestatus;
								break;
								
			case "ERROR":		$ro->requestreference = (string) $xmlResponse->requestreference;
								$ro->type = "ERROR";
								$ro->timestamp = (string) $xmlResponse->response->timestamp;
								$ro->transactionreference = (string) $xmlResponse->response->transactionreference;
								$ro->errorcode = (string) $xmlResponse->response->error->code;
								$ro->errormessage = (string) $xmlResponse->response->error->message;
								$ro->errordata = (string) $xmlResponse->response->error->data;
								break;
								
			case "REFUND":		$ro->requestreference = (string) $xmlResponse->requestreference;
								$ro->type="REFUND";
								$ro->merchantname = (string) $xmlResponse->response->merchant->merchantname;
								$ro->orderreference = (string) $xmlResponse->response->merchant->orderreference;
								$ro->tid = (string) $xmlResponse->response->merchant->tid;
								$ro->merchantnumber = (string) $xmlResponse->response->merchant->merchantnumber;
								$ro->merchantcountryiso2a = (string) $xmlResponse->response->merchant->merchantcountryiso2a;
								$ro->transactionreference = (string) $xmlResponse->response->transactionreference;
								$ro->amount = (string) $xmlResponse->response->billing->amount;
								$ro->currencycode = (string) $xmlResponse->response->billing->amount->attributes()->currencycode;
								$ro->cardtype = (string) $xmlResponse->response->billing->payment->attributes()->type;
								$ro->maskedpan = (string) $xmlResponse->response->billing->payment->pan;
								$ro->authcode = (string) $xmlResponse->response->authcode;
								$ro->timestamp = (string) $xmlResponse->response->timestamp;	
								$ro->securitycode = (string) $xmlResponse->response->security->securitycode;
								$ro->securitypostcode = (string) $xmlResponse->response->security->postcode;
								$ro->securityaddress = (string) $xmlResponse->response->security->address;
								$ro->parenttransactionreference = (string) $xmlResponse->response->operation->parenttransactionreference;
								$ro->accounttypedescription = (string) $xmlResponse->response->operation->accounttypedescription;
								$ro->settleduedate = (string) $xmlResponse->response->settlement->settleduedate;
								$ro->settlestatus = (string) $xmlResponse->response->settlement->settlestatus;
								break;
								
			case "TRANSACTIONUPDATE":
								$ro->timestamp = (string) $xmlResponse->response->timestamp;
								break;
								
		}	
		return $ro;
	}
}