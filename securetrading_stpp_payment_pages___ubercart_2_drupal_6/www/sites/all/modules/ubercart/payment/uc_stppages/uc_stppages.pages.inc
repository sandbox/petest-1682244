<?php

require_once(dirname(__FILE__) . '/securetrading_stpp/STPPLoader.php');
require_once(dirname(__FILE__) . '/UberCart24PPages.class.php');

function uc_stppages_complete($cart_id = 0) {

	$ob = new UberCart24PPages();
	
	if (!isset($_GET['responsesitesecurity']) && variable_get('uc_stppages_use_sitesecurity', TRUE)) {
		$ob->createException(new Exception('The responsesitesecurity was not returned but the site security hash was enabled.'), __FILE__, __CLASS__, __LINE__);
	}

	if (isset($_GET['responsesitesecurity']) && hash('sha256', variable_get('uc_stppages_sitesecurity', TRUE)) != $_GET['responsesitesecurity']) {
		$ob->createException(new Exception('The responsesitesecurity did not match the regenerated redirect hash.'), __FILE__, __CLASS__, __LINE__);
	}

	unset($ob);
	
	$page = variable_get('uc_cart_checkout_complete_page', '');
	
	if (!empty($page)) {
		drupal_goto($page);
	}
	
	uc_cart_empty(uc_cart_get_id());
	unset($_SESSION['cart_order'], $_SESSION['do_complete'], $_SESSION['new_user']);
	
	$output_message = '<p>Your order has been completed successfully.</p><p>Click here to return to the homepage.</p>';
	$themed_output = theme('uc_cart_complete_sale', $output_message);
	return $themed_output;
}