<?php

/**
 * SecureTrading STPP Shopping Carts
 * STPP Cart Interface Version 1.0.0
 * Last updated 10/10/2011
 * Written by Peter Barrow for SecureTrading Ltd.
 * http://www.securetrading.com
 */
 
 if (!class_exists('AbstractSTPP')) {
	require_once(dirname(__FILE__) . '/AbstractSTPP.class.php');
	require_once(dirname(__FILE__) . '/AbstractAPI.class.php');
	require_once(dirname(__FILE__) . '/AbstractSTAPI.class.php');
	require_once(dirname(__FILE__) . '/AbstractPaymentPages.class.php');
	require_once(dirname(__FILE__) . '/STPPXml.class.php');
	require_once(dirname(__FILE__) . '/AbstractSTPPHelper.class.php');
}

?>