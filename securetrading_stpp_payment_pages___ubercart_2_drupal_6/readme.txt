-------------
1.1. Contents
-------------

1.1. Contents
1.2. About
2.1. Installation
2.2. Configuration
2.3. Notification
2.4. Redirect
2.5. Site Security
2.6. Notification Hash
2.7. Expected Behaviour
3.1. Support

----------
1.2. About
----------

This SecureTrading UberCart Payment Pages module has been developed for the following specifications:

UberCart Version: 2.4
PHP Version: 5.0

Module Version: 2.5.0

-----------------
2.1. Installation
-----------------

Installation of the plugin is extremely simple.

1 - Copy and paste www/ folder into your root UberCart installation.
2 - If you are prompted with a box asking you if you would like to merge the folders, click 'Yes'.
3 - Login to Drupal as an administrator.
4 - Select 'Administer' in the admin panel.
5 - Click 'Modules' in the content area to the right.
6 - Scroll down to find the 'SecureTrading Payment Pages' module.  Tick the checkbox and click 'Save Configuration'.

The shopping cart should now be installed and ready to configure.

-------------------
2.2. Configuration
-------------------

To configure the plugin you should perform the following steps:

1 - Head to 'Administer' - 'Store Administration'.
2 - Click 'Configuration' in the main content area.
3 - Click 'Payment Settings' and then click on 'Payment Methods'.
4 - Click on 'SecureTrading Payment Pages Settings'.
5 - A list of configurable options will become visible.  Fill in these values accordingly and then click 'Save Configuration'.

Module Name - This is the name of this payment method as it will appear in the front-end checkout to your customers.

Notification Hash - This is the notification hash.  This option is discussed further in section 2.6.

Settle Due Date - This is the number of days you wish to wait before retrieving funds from your customers' account.  The default value is "Process Immediately".

Settle Status - This is the settlement status that will be applied to all successful transactions.  The default status is "0".

Site Reference - This is the Site Reference provided to you by SecureTrading.  If you do not have a Site Reference then please contact our Support department on +44 (0) 1248 672 050

Site Security - This is the Site Security hash.  This option is discussed further in section 2.5.

Use Notification Hash - Select this option to enable the notification hash.  This option is discussed further in section 2.6.

Use Site Security - Select this option to enable the Site Security hash.  This option is discussed further in section 2.5.

------------------
2.3.  Notification
------------------

You must set-up  a notification in MyST before this plugin can work correctly.

This feature is responsible for updating order information in the UberCart back-end after payment has been made via the SecureTrading Payment Gateway.

To set-up a notification you should perform the following steps:

1 - Login to MyST and select "Notifications".
2 - Select 'Add Filter' and then enter the following information:

FILTERS

  Description:
     Enter a recognizable name of your choice here.

  Requests:
     Auth

  Payment Types:
     Select all card types that you will be accepting here.

  Error Codes:
     All Error Codes

3 - Click 'Save'.
4 - Select 'Add Destination and enter the following information:

DESTINATIONS

  Description:
     Enter a recognizable name of your choice here.

  Notification Type:
     URL

  Process Notification:
     Online

  Destination:
    <your_root_ubercart_install_here>/cart/stppages/notification

  Security Password:
     This is the field that you will enter your chosen notification hash into.  See section 2.6 for more information.

  Security Algorithm:
     sha256

  Fields:
     errorcode
     orderreference
     securityresponseaddress
     securityresponsepostcode
     securityresponsesecuritycode

  Custom Fields:
     None

5 - Click 'Save'.
6 - Select the filter and destination you have just created from the two initially blank select menus (under 'Current Notifications').
7 - Click 'Save'.

The notification will now have been enabled.

-------------
2.4. Redirect
-------------

You must set-up a redirect before this plugin will work correctly.

To do this, contact SecureTrading Support on +44 (0) 1248 672 050.  Inform them that you would like to establish a redirect.

When prompted, inform them of the following information:

Redirect URL:
    <your_root_ubercart_install_here>/cart/stppages/complete

Fields:
     None

------------------
2.5. Site Security
------------------

A security code will prevent malicious users from modifying sensitive payment information before being directed to the payment screen.

This feature can be enabled by following these steps:

1 - Head to the Securetrading module's configuration.
2 - Select 'Yes' below 'Enable Site Security'.
3 - Enter a hard to guess combination of letters, numbers and symbols into the 'Site Security Password' field.  This combination should be at least 8 characters long.
4 - Click 'save'.
5  - You must now contact Support on +44 (0) 1248 672 050 and inform them that you have "enabled the Site Security Password Hash".  When prompted for a list of "enabled fields", you must tell them
the following, in this order:

     sitereference
     currencyiso3a
     mainamount
     orderreference
     PASSWORD *

'Site Security' is now enabled.  Remember to never tell any other individuals your Site Security Password.  Do not store hard copies of this password anywhere.

* The last field, 'PASSWORD', is to be the combination of characters you entered into the 'Site Security Password'.

----------------------
2.6. Notification Hash
----------------------

It is highly recommended that you enable the notification hash.  See section 2.3 for more information on the notification script itself.

To enable the notification hash you will firstly have to enter a 'Security Password' in the 'Destination' window of the MyST Notification page (see section 2.3).
You will also have to enter the same password into the UberCart Payment Pages configuration page.  Details on how to configure the module were given in section 2.2.

-----------------------
2.7. Expected Behaviour
-----------------------

New (incomplete) orders will be given the status 'In checkout'.

Successful orders will be given the status 'Pending'.

Declined and failed orders will be given the status 'Canceled'.

AVS Information will be reported under the 'Payments' tab in the single view order screen ('Home' - 'Administer' - 'Store administration' - 'orders' - [Select an order here]).

------------
3.1. Support
------------

If you require any assistance then please contact us immediately.

When contacting our Support line  you should search the www/sites/all/modules/ubercart/payment/uc_stppages directory for a logs/log.txt file.  If this file exists then please submit it with your initial support request.

http://www.securetrading.com/contact.html
support@securetrading.com
